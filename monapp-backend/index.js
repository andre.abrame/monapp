import express from "express";
import cors from "cors";

const app = express();
app.use(cors({
    origin: "http://localhost:5173"
}));

const people = [
    { id: 1, name: "andre", age: 40},
    { id: 2, name: "damien", age: 35},
    { id: 3, name: "thierry", age: 27},
    { id: 4, name: "ahlam", age: 22},
];

// find all
app.get("/people", (req, res) => {
    res.json(people);
});

// find by id
app.get("/people/:id", (req, res) => {
    const id = parseInt(req.params.id);
    for (const person of people) {
        if (person.id === id) {
            res.json(person);
            return;
        }
    }
    res.status(404).json("no person with id " + id + " exists");
});

app.listen(3000, () => console.log("started on port 3000"));
