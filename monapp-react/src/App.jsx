import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Header from "./components/Header";
import Users from "./pages/Users";
import Login from "./pages/Login";
import UserProfile from "./pages/UserProfile";
import { Route, Routes } from "react-router-dom";

function App() {
  const [user, setUser] = useState("");
  return (
    <>
      <div className="min-vh-100 min-vw-100">
        <Header user={user} />
        {/* <h1 className="alert alert-info">Coucou</h1> */}
        {/* <Login setUser={setUser} /> */}
        <div className="p-3">
          <Routes>
            <Route index element={<Users />} />
            <Route path="login" element={<Login setUser={setUser} />} />
            <Route path="/users/:id" element={<UserProfile />} />
          </Routes>
        </div>
      </div>
    </>
  );
}

export default App;