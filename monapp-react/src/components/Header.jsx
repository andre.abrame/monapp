import React from "react";
import logo from "../assets/react.svg";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

function Header(props) {
  const message = props.user ? (
    <div>Bienvenue {props.user} !</div>
  ) : (
    <div>Connectez-vous !</div>
  );
  return (
    <div>
      <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
        <div className="container-fluid">
          <div className="navbar-brand">
            <img
              src={logo}
              alt=""
              width="30"
              height="24"
              className="d-inline-block align-text-top mt-1"
            />
            Formation React
          </div>
          <ul className="navbar-nav me-auto">
            <li className="nav-item">
              <Link className="nav-link" to="/">
                Accueil
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/login">
                Login
              </Link>
            </li>
          </ul>
          <div className="navbar-text">{message}</div>
        </div>
      </nav>
    </div>
  );
}

Header.propTypes = {
  user: PropTypes.string.isRequired,
};

export default Header;