import React from "react";
import PropTypes from "prop-types";

function Search(props) {
  return (
    <input
      type="text"
      className="form-control"
      placeholder="Filtrage de la liste"
      onChange={(event) => props.onChange(event.target.value)}
    />
  );
}

Search.propTypes = {
  onChange: PropTypes.func.isRequired,
};

export default Search;