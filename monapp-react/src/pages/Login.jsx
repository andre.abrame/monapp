import React, { useCallback, useState } from "react";
import PropTypes from "prop-types";

function Login(props) {
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const handleForm = useCallback(
    (event) => {
      event.preventDefault();
      console.log("Tu as saisie : " + login + " et " + password);
      props.setUser(login);
    },
    [login, password]
  );
  return (
    <div className="d-flex justify-content-center">
      <div className="col-12 col-sm-10 col-md-8 col-lg-4">
        <h2 className="text-center">Connexion</h2>
        <form onSubmit={handleForm}>
          <label htmlFor="id">Identifiant</label>
          <input
            className="form-control"
            id="id"
            value={login}
            onChange={(event) => setLogin(event.target.value)}
          />
          <label htmlFor="password">Mot de passe</label>
          <input
            className="form-control"
            id="password"
            type="password"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
          />
          <div className="d-grid mt-4">
            <button className="btn btn-info">Valider</button>
          </div>
        </form>
      </div>
    </div>
  );
}

Login.propTypes = {
  setUser: PropTypes.func.isRequired,
};

export default Login;
