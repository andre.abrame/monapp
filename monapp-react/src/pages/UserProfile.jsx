import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

function UserProfile() {
  const { id } = useParams();
  const [user, setUser] = useState({});
  useEffect(() => {
    const fetchdData = async () => {
      const res = await fetch(
        "http://localhost:3000/people/" + id
      );
      const user = await res.json();
      setUser(user);
    };
    fetchdData();
  }, []);
  return (
    <>
      <p>
        Nom : {user.name} <br />
        Age: {user.age} <br />
      </p>
    </>
  );
}

export default UserProfile;