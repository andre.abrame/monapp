import React, { useEffect, useState } from "react";
import Search from "../components/Search";
import { Link } from "react-router-dom";

function Users() {
  const [filter, setFilter] = useState("");
  const [users, setUsers] = useState([]);
  const [usersFiltered, setUsersFiltered] = useState(users);
  useEffect(() => {
    setUsersFiltered(
      users.filter((user) =>
        user.name.toLowerCase().includes(filter.toLocaleLowerCase())
      )
    );
  }, [filter, users]);
  useEffect(() => {
    const fetchdData = async () => {
      try {
        const response = await fetch(
          "http://localhost:3000/people"
        );
        const users = await response.json();
        setUsers(users);
      } catch (err) {
        console.error(err);
      }
    };
    fetchdData();
  }, []);
  return (
    <>
      <Search onChange={setFilter} />
      <ul>
        {users.length ? (
          usersFiltered.map((user, index) => (
            <li key={index}>
              <Link to={"/users/" + user.id}>{user.name}</Link>
            </li>
          ))
        ) : (
          <div className="d-flex justify-content-center align-items-center mt-3">
            <div className="spinner-border text-primary" role="status">
              <span className="visually-hidden">Loading...</span>
            </div>
          </div>
        )}
      </ul>
    </>
  );
}
export default Users;